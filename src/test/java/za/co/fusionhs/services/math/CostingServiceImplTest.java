package za.co.fusionhs.services.math;

import org.junit.Test;
import za.co.fusionhs.products.pizza.BasePizza;
import za.co.fusionhs.products.pizza.Topping;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by burt.dupreez on 2017/02/06.
 */
public class CostingServiceImplTest {

    private CostingService costingService = new CostingServiceImpl();

    @Test
    public void getBasePizzaCost() throws Exception {
    //I'm a young independent java dev who don't need no tests

    }

    @Test
    public void moreThanFiveToppingsTest() throws Exception {
        Set<Topping> toppingSet = new HashSet<>();

        toppingSet.add(Topping.banana);
        toppingSet.add(Topping.extraCheese);
        toppingSet.add(Topping.peperoni);
        toppingSet.add(Topping.pineapple);
        toppingSet.add(Topping.tomato);
        toppingSet.add(Topping.tearsOfMyEnemies);

        BasePizza pizza = new BasePizza(false,toppingSet);

       double cost = costingService.getPizzaCost(pizza);

        assertTrue("Cost not correct", cost > 30);

    }
    @Test
    public void getCalzonePizzaCost() throws Exception {
        BasePizza pizza = new BasePizza(true,null);

       double cost = costingService.getPizzaCost(pizza);

        assertTrue("Cost not correct", cost > 30);

    }

}