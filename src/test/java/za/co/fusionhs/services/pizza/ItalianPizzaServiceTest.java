package za.co.fusionhs.services.pizza;

import org.junit.Test;
import za.co.fusionhs.products.pizza.BasePizza;

import static org.junit.Assert.*;

/**
 * Created by burt.dupreez on 2017/02/06.
 */
public class ItalianPizzaServiceTest {
    PizzaService pizzaService = new ItalianPizzaService();
    @Test
    public void createPizza() throws Exception {
        BasePizza pizza = pizzaService.createPizza(null);
        assertFalse("Must be not calzone", pizza.isCalzone());

    }

    @Test
    public void createCalzonePizza() throws Exception {
        BasePizza pizza = pizzaService.createCalzonePizza(null);

        assertTrue("Must be calzone", pizza.isCalzone());

    }

}