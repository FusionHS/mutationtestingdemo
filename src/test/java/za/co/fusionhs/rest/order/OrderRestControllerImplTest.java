package za.co.fusionhs.rest.order;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.fusionhs.products.pizza.BasePizza;
import za.co.fusionhs.products.pizza.Topping;
import za.co.fusionhs.services.math.CostingService;
import za.co.fusionhs.services.pizza.PizzaService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

/**
 * Created by burt.dupreez on 2017/02/06.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderRestControllerImplTest {

    @SpyBean
    private CostingService costingMockService;
    @SpyBean
    private PizzaService pizzaMockService;
    @SpyBean
    private OrderRestController orderRestController;

    @Test
    public void orderBasePizza() throws Exception {

        BasePizza pizza = new BasePizza(false,null);

        doReturn(pizza).when(pizzaMockService).createPizza(null);
        doReturn(30d).when(costingMockService).getPizzaCost(any(BasePizza.class));
        double cost = orderRestController.orderBasePizza(false,null);

        assertEquals(30, cost, 0.0001);
    }

    @Test
    public void orderCalzonePizza() throws Exception {

        BasePizza pizza = new BasePizza(true,null);
        doReturn(pizza).when(pizzaMockService).createCalzonePizza(null);
        doReturn(40d).when(costingMockService).getPizzaCost(any(BasePizza.class));

        orderRestController.orderBasePizza(true,null);
    }

    @Test
    public void addTopping() throws Exception {

        BasePizza pizza = new BasePizza(false,null);

        double cost = orderRestController.addTopping(pizza, Topping.tearsOfMyEnemies);
        assertTrue("Cost not correct", cost >= 33);
    }

}