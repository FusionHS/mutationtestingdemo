package za.co.fusionhs.services.math;

import org.springframework.stereotype.Service;
import za.co.fusionhs.products.pizza.BasePizza;

/**
 * Created by burt.dupreez on 2017/02/06.
 */
@Service
public class CostingServiceImpl implements CostingService {


    @Override
    public double getPizzaCost(BasePizza pizza) {
        double cost = pizza.getBaseCost();
        if (pizza.isCalzone() || pizza.getNumberOfToppings() == 0) {
            cost = cost + 10;
        }

        cost += (pizza.getNumberOfToppings() * 3);//SPECIAL ALL TOPPINGS ARE R3!

        //5% discount
        if (pizza.getNumberOfToppings() >= 5) {
            cost *= 0.95;
        }

        return cost;
    }
}
