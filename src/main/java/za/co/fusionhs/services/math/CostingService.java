package za.co.fusionhs.services.math;

import za.co.fusionhs.products.pizza.BasePizza;

/**
 * Created by burt.dupreez on 2017/02/06.
 */
public interface CostingService {
    double getPizzaCost(BasePizza pizza);
}
