package za.co.fusionhs.services.pizza;

import org.springframework.stereotype.Service;
import za.co.fusionhs.products.pizza.BasePizza;
import za.co.fusionhs.products.pizza.Topping;

import java.util.Set;

/**
 * Created by burt.dupreez on 2017/02/06.
 */
@Service
public class ItalianPizzaService implements PizzaService {


    @Override
    public BasePizza createPizza(Set<Topping> toppings) {
        return new BasePizza(false, toppings);
    }

    @Override
    public BasePizza createCalzonePizza(Set<Topping> toppings) {
        return new BasePizza(true, toppings);
    }

    @Override
    public boolean addPizzaTopping(BasePizza pizza, Topping topping) {
      pizza.addTopping(topping);
      return true;
    }
}
