package za.co.fusionhs.services.pizza;

import za.co.fusionhs.products.pizza.BasePizza;
import za.co.fusionhs.products.pizza.Topping;

import java.util.Set;

/**
 * Created by burt.dupreez on 2017/02/06.
 */
public interface PizzaService {

    BasePizza createPizza(Set<Topping> toppings);

    BasePizza createCalzonePizza(Set<Topping> toppings);

    boolean addPizzaTopping(BasePizza pizza, Topping topping);
}
