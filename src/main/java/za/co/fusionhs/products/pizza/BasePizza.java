package za.co.fusionhs.products.pizza;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by burt.dupreez on 2017/02/06.
 */
public class BasePizza {
    private static final double BASE_COST = 30;
    private final boolean calzone;
    private Set<Topping> toppingList;

    public BasePizza(boolean calzone, Set<Topping> toppingList) {
        this.calzone = calzone;

        if (toppingList != null) {
            this.toppingList = toppingList;
        } else {
            this.toppingList = new HashSet<>();
        }
    }

    public double getBaseCost() {
        return BASE_COST;
    }

    public void addTopping(Topping topping) {
        toppingList.add(topping);
    }

    public boolean isCalzone() {
        return calzone;
    }

    public int getNumberOfToppings() {
        int toppingsCount = this.toppingList.size();
        if (calzone) {
            toppingsCount++;
        }
        return toppingsCount;
    }
}
