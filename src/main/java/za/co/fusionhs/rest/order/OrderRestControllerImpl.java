package za.co.fusionhs.rest.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.fusionhs.products.pizza.BasePizza;
import za.co.fusionhs.products.pizza.Topping;
import za.co.fusionhs.services.math.CostingService;
import za.co.fusionhs.services.pizza.PizzaService;

import java.util.Set;

/**
 * Created by burt.dupreez on 2017/02/06.
 */
@RestController
public class OrderRestControllerImpl implements OrderRestController {


    private CostingService costingService;

    private PizzaService pizzaService;

    @Autowired
    public OrderRestControllerImpl(CostingService costingService, PizzaService pizzaService) {
        this.costingService = costingService;
        this.pizzaService = pizzaService;
    }

    @RequestMapping("/orderPizza")
    public double orderBasePizza(boolean calzone, Set<Topping> toppings) {

        BasePizza pizza;
        if (calzone) {
            pizza = pizzaService.createCalzonePizza(toppings);
        } else {
            pizza = pizzaService.createPizza(toppings);
        }

        return costingService.getPizzaCost(pizza);
    }
    @RequestMapping("/addTopping")
    public double addTopping(BasePizza pizza, Topping topping) {

        pizza.addTopping(topping);
        pizzaService.addPizzaTopping(pizza,topping);
        return costingService.getPizzaCost(pizza);
    }


}
