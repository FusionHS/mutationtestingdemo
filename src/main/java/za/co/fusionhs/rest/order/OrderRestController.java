package za.co.fusionhs.rest.order;

import za.co.fusionhs.products.pizza.BasePizza;
import za.co.fusionhs.products.pizza.Topping;

import java.util.Set;

/**
 * Created by burt.dupreez on 2017/02/06.
 */
public interface OrderRestController {
    double orderBasePizza(boolean calzone, Set<Topping> toppings);

    double addTopping(BasePizza pizza, Topping topping);
}
